# Variables defined outside the scope of a job apply to all jobs
# Variables defined inside the scope of a job apply to that job only
variables:
  GIT_DEPTH: 0
  ARTIFACT_DIR: build

# stages is a list of job "stages" that will run sequentially
# In the below case 1) run-test 2) lint and 3) scan
# Each stage can be composed of several jobs that will run in parallel

stages:
- build
- test
- lint
- scan
- vulnerability-report


# Job names can be the same as stage name ("build-python" could be "build" here)
# Jobs can be in any order in the YAML, the "stages" and/or "needs" specifications define the order
build-python:
  stage: build
  # Use python:3.9 to make sure the code is backward compatible with older python versions
  image: python:3.9
  before_script:
    # Install packaging tool
    - pip install wheel
  script:
    # The below builds the python package
    - python setup.py bdist_wheel
  only:
    refs:
      - merge_requests
      - master
      - develop

# lint-flake8 is a job (arbitrary name)
lint-flake8:
  # stage below is the stage this job belongs to
  stage: lint

  # Each job will run in a separate docker image
  # You should select the image that's best for the job
  image: python:latest

  # Before script
  before_script:
    - pip install flake8
  script:
    - mkdir -p ${ARTIFACT_DIR}
    # If any command in the script returns a non 0 code
    # the job is failed, but the pipeline may proceed if allow_failure

    # Flake8 return non 0 code that fails the artifacts passing
    # Hence the --exit-zero option
    - flake8 --exit-zero --ignore=W503,E128,C901,W504,E302,E265,E741,W291,W293,W391 --max-line-length=150 . > ${ARTIFACT_DIR}/flake8-report.out
  # If allow_failure: the pipeline stops if the job fails (any
  # command in the job return a non 0 code)
  allow_failure: true
  only:
    refs:
      - merge_requests
      - master
      - develop
  # artifacts defines files and dirs that should be preserved
  # Throughout the build. If not specified, artifacts of a job
  # are not available to the next job (ie the flake8 linter report here)
  # If a job fails, with allow_failure: true, artifacts are also NOT
  # available to the next jobs. Hence the tricks to return 0 code
  # for non fatal job commands
  artifacts:
    paths:
      - ${ARTIFACT_DIR}/
  needs:
    - job: build-python

test-pytest:
  stage: test
  image: python:latest
  before_script:
    - pip install pytest coverage
  script:
    - mkdir -p ${ARTIFACT_DIR}
    # Pytest would return code 1 if some test fail. Avoid job failure in that case
    # Another trick here to return code 0 for non fatal errors
    - coverage run -m pytest || [ $? -eq 1 ]
    - coverage xml -o ${ARTIFACT_DIR}/coverage.xml
  allow_failure: true
  only:
    refs:
      - merge_requests
      - master
      - develop
  artifacts:
    paths:
      - ${ARTIFACT_DIR}/
  needs:
    - job: build-python

lint-pylint:
  stage: lint
  # Use Python docker image for python tools (linters and tests)
  image: python:latest
  before_script:
    - pip install pylint pylint-exit
  script:
    - mkdir -p ${ARTIFACT_DIR}
    # pylint-exit is a tool that converts pylint non fatal error code to 0
    # to proceed with the pipeline and the artifacts
    - pylint src --disable=C0114,C0116 -r n --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" > ${ARTIFACT_DIR}/pylint-report.out || pylint-exit $?
    - cat ${ARTIFACT_DIR}/pylint-report.out|grep "has been rated"|cut -d ' ' -f 7|cut -d '/' -f 1 >${ARTIFACT_DIR}/pylint-rating.txt
  allow_failure: true
  only:
    refs:
      - merge_requests
      - master
      - develop
  artifacts:
    paths:
      - ${ARTIFACT_DIR}/
  needs:
    - job: build-python

# Settings for SonarQube
scan-cli:
  image:
    # Use different docker image for scanner
    name: sonarsource/sonar-scanner-cli:latest
  cache:
    # Use a cache key that's common to all jobs running the scanner
    key: "sonarscanner-${CI_PROJECT_NAME}"
    paths:
      - .sonar/cache
  stage: scan
  needs:
    # Specify which earlier jobs artifacts are needed
    - job: lint-pylint
      # artifacts: true # Default
    - job: lint-flake8
    - job: test-pytest
  # before_script:
  #   - apk add --update --no-cache python3 py3-pip
  #   - pip install sonar-tools
  #   - apk add curl
  #   # To get minimum Linux cmd line tools (eg "tail")
  #   - apk add coreutils
  script:
    - ls -l ${ARTIFACT_DIR}
    - cat ${ARTIFACT_DIR}/*.out ${ARTIFACT_DIR}/*.txt ${ARTIFACT_DIR}/*.xml
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.qualitygate.timeout=300 -X
  allow_failure: false
  only:
    refs:
      - merge_requests
      - master
      - develop

vulnerability-report:
  stage: vulnerability-report
  script:
    - 'curl -u "${SONAR_TOKEN}:" "${SONAR_HOST_URL}/api/issues/gitlab_sast_export?projectKey=demo:gitlab:scanner-cli&branch=${CI_COMMIT_BRANCH}&pullRequest=${CI_MERGE_REQUEST_IID}" -o gl-sast-sonar-report.json'
  allow_failure: true
  only:
    - merge_requests
    - master
    - main
    - develop
  artifacts:
    expire_in: 1 day
    reports:
      sast: gl-sast-sonar-report.json
  dependencies:
    - scan-cli